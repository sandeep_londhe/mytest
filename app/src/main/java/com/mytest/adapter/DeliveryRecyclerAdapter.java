package com.mytest.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Sandeep Londhe on 18-09-2018.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mytest.R;
import com.mytest.model.DeliveryResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DeliveryRecyclerAdapter extends RecyclerView.Adapter<DeliveryRecyclerAdapter.MyViewHolder> {


    private List<DeliveryResponse> deliveryResponseList;
    Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView description;
        public ImageView mainImage;

        public MyViewHolder(View view) {
            super(view);
            description = (TextView) view.findViewById(R.id.listDescription);
            mainImage = (ImageView) view.findViewById(R.id.listImage);
        }
    }


    public DeliveryRecyclerAdapter(List<DeliveryResponse> deliveryResponseList, Activity activity) {
        this.deliveryResponseList = deliveryResponseList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DeliveryResponse deliveryResponse = deliveryResponseList.get(position);
        holder.description.setText(deliveryResponse.getDescription());


        Picasso.with(activity)
                .load(deliveryResponse.getImageUrl())
                .into(holder.mainImage);

    }

    @Override
    public int getItemCount() {
        return deliveryResponseList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
