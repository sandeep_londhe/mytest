package com.mytest.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.android.gms.tasks.Task;

import java.util.List;

/**
 * Created by Sandeep Londhe on 19-09-2018.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */

@Dao
public interface DaoDelivery {


    @Query("SELECT * FROM delivery")
    List<Delivery> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Delivery delivery);

    @Delete
    void delete(Delivery delivery);

    @Update
    void update(Delivery delivery);
}
