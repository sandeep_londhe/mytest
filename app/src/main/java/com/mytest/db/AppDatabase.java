package com.mytest.db;

/**
 * Created by Sandeep Londhe on 19-09-2018.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Delivery.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DaoDelivery daoDelivery();
}

