package com.mytest.retro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mytest.utility.Constant;



/**
 * Created by Sandeep on 28-12-2017.
 */

public class ApiUtils {


    private ApiUtils() {
    }


    public static final String BASE_URL = Constant.HOST_URL;

    public static MainAPIInterface getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(MainAPIInterface.class);
    }

}
