package com.mytest.retro;

import com.mytest.model.DeliveryResponse;
import com.mytest.utility.Constant;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Sandeep Londhe on 01-09-2018.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */

public interface MainAPIInterface<R extends Retrofit> {

    @GET(Constant.DELIVERIES)
    Call<List<DeliveryResponse>> getAllDeliveries();


}
