package com.mytest.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mytest.R;
import com.mytest.adapter.DeliveryRecyclerAdapter;
import com.mytest.db.DatabaseClient;
import com.mytest.db.Delivery;
import com.mytest.model.DeliveryResponse;
import com.mytest.retro.ApiUtils;
import com.mytest.retro.MainAPIInterface;
import com.mytest.utility.AppLoadingDialog;
import com.mytest.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private List<DeliveryResponse> deliveryResponseList = new ArrayList<>();
    private DeliveryRecyclerAdapter mAdapter;

    RecyclerView listDeliveries;

    MainAPIInterface mainAPIInterface;

    private AppLoadingDialog progressDialog;


    DeliveryResponse.Location mLocation;

    Delivery delivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainAPIInterface = ApiUtils.getAPIService();

        delivery = (Delivery) getIntent().getSerializableExtra("delivery");


        listDeliveries = (RecyclerView) findViewById(R.id.listDeliveries);

        if (isNetworkAvailable(MainActivity.this)) {

            //  deleteDelivery(delivery);
            getAllDeliveryRequest();


        } else {

            GetDeliveryOffline gt = new GetDeliveryOffline();
            gt.execute();

            //Toast.makeText(getApplicationContext(), "Check internet connection.!", Toast.LENGTH_SHORT).show();

        }


        listDeliveries.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), listDeliveries, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                if (isNetworkAvailable(MainActivity.this)) {


                    DeliveryResponse deliveryResponse = deliveryResponseList.get(position);
                    DeliveryResponse.Location location = deliveryResponse.getLocation();
                    Intent i = new Intent(MainActivity.this, MapsActivity.class);
                    i.putExtra("des", deliveryResponse.getDescription());
                    i.putExtra("image", deliveryResponse.getImageUrl());
                    i.putExtra("latitude", location.getLat().toString());
                    i.putExtra("longitude", location.getLng().toString());
                    i.putExtra("address", location.getAddress());
                    startActivity(i);
                    Toast.makeText(getApplicationContext(), deliveryResponse.getDescription() + " is selected!", Toast.LENGTH_SHORT).show();


                } else {


                    GetDeliveryOffline gt = new GetDeliveryOffline();
                    gt.execute();

                    DeliveryResponse deliveryResponse = deliveryResponseList.get(position);
                    DeliveryResponse.Location location = deliveryResponse.getLocation();


                    Intent i = new Intent(MainActivity.this, MapsActivity.class);
                    i.putExtra("des", deliveryResponse.getDescription());
                    i.putExtra("image", deliveryResponse.getImageUrl());
                    i.putExtra("latitude", location.getLat().toString());
                    i.putExtra("longitude", location.getLng().toString());
                    i.putExtra("address", location.getAddress());
                    startActivity(i);
                    Toast.makeText(getApplicationContext(), deliveryResponse.getDescription() + " is selected!", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }


    private void getAllDeliveryRequest() {

        startProgress();


        mainAPIInterface.getAllDeliveries().enqueue(new Callback<List<DeliveryResponse>>() {
            @Override
            public void onResponse(Call<List<DeliveryResponse>> call, Response<List<DeliveryResponse>> response) {

                stopProgress();

                if (response.isSuccessful()) {

                    deliveryResponseList = response.body();

                    System.out.println("Check Success==" + response.body().toString());

                    mAdapter = new DeliveryRecyclerAdapter(deliveryResponseList, MainActivity.this);

                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);

                    listDeliveries.setLayoutManager(layoutManager);

                    listDeliveries.setItemAnimator(new DefaultItemAnimator());
                    listDeliveries.setAdapter(mAdapter);


                    SaveDelivery st = new SaveDelivery();
                    st.execute();
                }
            }

            @Override
            public void onFailure(Call<List<DeliveryResponse>> call, Throwable t) {
                stopProgress();
                Log.i("Error", t.getMessage().toString());
            }
        });

    }


    class GetDeliveryOffline extends AsyncTask<Void, Void, List<Delivery>> {

        @Override
        protected void onPreExecute() {

//            deliveryResponseList.clear();
            Log.e("onPreExecutive", "called");
        }

        @Override
        protected List<Delivery> doInBackground(Void... voids) {
            List<Delivery> deliveriesList = DatabaseClient
                    .getInstance(getApplicationContext())
                    .getAppDatabase()
                    .daoDelivery()
                    .getAll();
            return deliveriesList;
        }

        @Override
        protected void onPostExecute(List<Delivery> deliveries) {
            super.onPostExecute(deliveries);

            deliveryResponseList.clear();


            for (int i = 0; i < deliveries.size(); i++) {


                DeliveryResponse deliveryResponse = new DeliveryResponse();
                deliveryResponse.setDescription(deliveries.get(i).getDesc());
                deliveryResponse.setImageUrl(deliveries.get(i).getImg());

                DeliveryResponse.Location mLocation = new DeliveryResponse.Location();
                mLocation.setLng(Double.parseDouble(deliveries.get(i).getLon()));
                mLocation.setLat(Double.parseDouble(deliveries.get(i).getLat()));
                mLocation.setAddress(deliveries.get(i).getAddress());


                deliveryResponse.setLocation(mLocation);

                deliveryResponseList.add(deliveryResponse);


            }

            System.out.println("Check Success==" + deliveries.toString());

            mAdapter = new DeliveryRecyclerAdapter(deliveryResponseList, MainActivity.this);

            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);

            listDeliveries.setLayoutManager(layoutManager);

            listDeliveries.setItemAnimator(new DefaultItemAnimator());
            listDeliveries.setAdapter(mAdapter);


            mAdapter.notifyDataSetChanged();
        }
    }


    private void startProgress() {
        try {
            if (progressDialog == null) {
                progressDialog = new AppLoadingDialog(MainActivity.this);
            }
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopProgress() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    class SaveDelivery extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            //Save Data to Database


            for (int i = 0; i < deliveryResponseList.size(); i++) {
                //creating a task
                Delivery delivery = new Delivery();

                delivery.setId(deliveryResponseList.get(i).getId());
                delivery.setDesc(deliveryResponseList.get(i).description);
                delivery.setAddress(deliveryResponseList.get(i).getLocation().getAddress());
                delivery.setImg(deliveryResponseList.get(i).imageUrl);
                delivery.setLat(deliveryResponseList.get(i).getLocation().getLat().toString());
                delivery.setLon(deliveryResponseList.get(i).getLocation().getLng().toString());

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .daoDelivery()
                        .insert(delivery);


            }


            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //          finish();
//            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }


    public static boolean isNetworkAvailable(Context mContext) {
        Context context = mContext.getApplicationContext();
        ConnectivityManager connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
