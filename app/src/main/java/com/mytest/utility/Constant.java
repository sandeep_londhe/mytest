package com.mytest.utility;

/**
 * Created by Sandeep Londhe on 18-09-2018.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */
public class Constant {

    public static final String HOST_URL = "http://mock-api-mobile.dev.lalamove.com/";

    public static final String DELIVERIES = "deliveries";


}
